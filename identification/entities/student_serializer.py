from rest_framework import serializers
from identification.models import Student
from study.entities.course_serializer import CourseSerializer
from study.entities.course_model import Course
from identification.entities.user_serializer import UserSerializer
from identification.entities.student_card_serializer import StudentCardSerializer
from identification.models import StudentCard
from authorization.entities.study_field_serializer import StudyFieldSerializer

class StudentSerializer(serializers.ModelSerializer):
    user = UserSerializer(required = True)
    course = CourseSerializer(many = True)
    student_card = StudentCardSerializer()
    study_field = StudyFieldSerializer()
    class Meta:
        model = Student
        fields = ('user',
        'student_card',
        'course',
        'active_type',
        'education_type',
        'study_field',
        )
    
    def create(self,validated_data):
        courses_data = validated_data.pop('course')
        user_data = validated_data.pop('user')
        student_card_data = validated_data.pop('student_card')
        student_card_obj = StudentCard.objects.create(**student_card_data)
        user = UserSerializer.create(
            UserSerializer(),
            validated_data = user_data,
        )
        student, created = Student.objects.update_or_create(
          user = user,
        )
        student.student_card = student_card_obj
        for c in courses_data:
            course_data = Course.objects.create(**c)
            student.course.add(course_data)
        return student
