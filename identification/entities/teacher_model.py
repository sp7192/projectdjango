from django.db import models
from django.contrib.auth.models import User
from study.entities.course_model import Course

class Teacher(models.Model):
    user = models.OneToOneField(User,on_delete = 'CASCADE')
    course = models.ManyToManyField(Course)
    code = models.CharField(max_length = 150)
