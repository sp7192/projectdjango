from django.db import models
from django.contrib.auth.models import User
from study.entities.course_model import Course
from common.utils import StudentActiveType
from identification.entities.student_card_model import StudentCard
from authorization.entities.study_field_model import StudyField

class Student(models.Model):
    user = models.OneToOneField(User,on_delete = 'CASCADE')
    course = models.ManyToManyField(Course)
    student_card = models.OneToOneField(StudentCard,on_delete = 'CASCADE')
    active_type = models.CharField(choices=StudentActiveType.choices(), default=StudentActiveType.activated.value, max_length=20)
    education_type = models.BooleanField(default=True)
    study_field = models.ForeignKey(StudyField,null=True,on_delete = 'CASCADE')
