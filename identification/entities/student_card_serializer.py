from rest_framework import serializers
from identification.entities.student_card_model import StudentCard

class StudentCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCard
        fields = (
            'card_id',
            'card_code',
        )
