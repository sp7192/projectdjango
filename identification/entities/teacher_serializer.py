from rest_framework import serializers
from identification.models import Teacher
from study.entities.course_serializer import CourseSerializer
from study.entities.course_model import Course
from identification.entities.user_serializer import UserSerializer

class TeacherSerializer(serializers.ModelSerializer):
    user = UserSerializer(required = True)
    course = CourseSerializer(many = True)
    class Meta:
        model = Teacher
        fields = ('user','course','code')
        
    def create(self,validated_data):
        courses_data = validated_data.pop('course')
        user_data = validated_data.pop('user')
        user = UserSerializer.create(
            UserSerializer(),
            validated_data = user_data,
        )
        teacher, created = Teacher.objects.update_or_create(
            user = user,
        )
        for c in courses_data:
            course_data = Course.objects.create(**c)
            teacher.course.add(course_data)
        return teacher
