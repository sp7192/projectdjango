from django.conf.urls import url,include
from identification import views
from django.contrib import admin
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

admin.autodiscover()
router = routers.DefaultRouter()

urlpatterns = [
    url(r'^student/$',views.StudentList.as_view(),name = views.StudentList.name),
    url(r'^student/(?P<pk>[0-9]+)$',views.StudentDetail.as_view(),name = views.StudentDetail.name),
    url(r'^teacher/(?P<pk>[0-9]+)$',views.TeacherDetail.as_view(),name = views.TeacherDetail.name),
    url(r'^teacher/$',views.TeacherList.as_view(),name = views.TeacherList.name),
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
]

urlpatterns += format_suffix_patterns([
    url(r'^teacher/$',
        views.TeacherRecordView.as_view(),
        name='teacher_list'),
])
