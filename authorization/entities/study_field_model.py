from django.db import models
from common.utils import CourseTitlePeriodType
from common.utils import StudyDegree


class StudyField(models.Model):
    title = models.CharField(max_length = 150)
    code = models.CharField(max_length = 150)
    parents = models.ManyToManyField('StudyField',blank=True)
    period = models.CharField(choices=CourseTitlePeriodType.choices(), default=CourseTitlePeriodType.type0.value, max_length=20)
    degree = models.CharField(choices=StudyDegree.choices(), default=StudyDegree.Bachelor.value, max_length=20)
	
