from rest_framework import serializers
from authorization.models import StudyField

class StudyFieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudyField
        fields = '__all__'
