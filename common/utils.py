from enum import Enum

class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)

class StudentActiveType(ChoiceEnum):
	deactivated = 0
	activated = 1
	banned = 2

class CourseTitlePeriodType(ChoiceEnum):
	type0 = 0
	type1 = 1

class StudyDegree(ChoiceEnum):
	Bachelor = 0
	Master = 1
