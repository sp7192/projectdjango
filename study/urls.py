from django.conf.urls import url,include
from study import views
from django.contrib import admin


urlpatterns = [
    url(r'^course/$',views.CourseList.as_view(),name = views.CourseList.name),
    url(r'^course/(?P<pk>[0-9]+)$',views.CourseDetail.as_view(),name = views.CourseDetail.name),
    url(r'^term/$',views.TermList.as_view(),name = views.TermList.name),
    url(r'^term/(?P<pk>[0-9]+)$',views.TermDetail.as_view(),name = views.TermDetail.name),    
]
