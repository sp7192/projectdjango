from django.contrib import admin

from .models import Course
from .models import Term
from .models import CourseTitle

admin.site.register(Course)
admin.site.register(Term)
admin.site.register(CourseTitle)
# Register your models here.
