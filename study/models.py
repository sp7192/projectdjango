from study.entities.course_model import Course
from study.entities.term_model import Term
from study.entities.course_title_model import CourseTitle
# Create your models here.
assert Course
assert Term
assert CourseTitle
