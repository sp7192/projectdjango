from django.db import models


class Term(models.Model):
    title = models.CharField(max_length = 150)
    code = models.CharField(max_length = 150)
    begin = models.DateField()
    end = models.DateField()
