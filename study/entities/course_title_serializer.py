from rest_framework import serializers
from study.models import CourseTitle

class CourseTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseTitle
        fields = '__all__'
