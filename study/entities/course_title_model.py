from django.db import models
from common.utils import CourseTitlePeriodType

class CourseTitle(models.Model):
    title = models.CharField(max_length = 150)
    code = models.CharField(max_length = 150)
    parents = models.ManyToManyField('CourseTitle',blank=True)
    period = models.CharField(choices=CourseTitlePeriodType.choices(), default=CourseTitlePeriodType.type0.value, max_length=20)

