from rest_framework import serializers
from study.entities.course_model import Course
from study.entities.term_serializer import TermSerializer
from study.entities.course_title_serializer import CourseTitleSerializer


class CourseSerializer(serializers.ModelSerializer):
    term = TermSerializer()
    course_title = CourseTitleSerializer()
    class Meta:
        model = Course
        fields = (
            'course_name',
            'term',
            'course_title',
        )
