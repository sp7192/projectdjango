from django.db import models
from study.entities.term_model import Term
from study.entities.course_title_model import CourseTitle

class Course(models.Model):
    course_name = models.CharField(max_length = 150)
    term = models.ForeignKey(Term,null=True,on_delete='CASCADE')
    course_title = models.ForeignKey(CourseTitle,null=True,on_delete='CASCADE')
