from study.entities.course_serializer import CourseSerializer
from study.entities.term_serializer import TermSerializer
from study.entities.course_title_serializer import CourseTitleSerializer

assert CourseSerializer
assert TermSerializer
assert CourseTitleSerializer
